# WP_SATYA - PROYECTO DE TIENDA
## ACTUALIZACIÓN DEL SERVIDOR

C:\Users\Guest>nslookup
Servidor predeterminado:  SZGZADWK16V01.satyatec.net
Address:  172.16.0.25

> set q=a
> satyatec.es
Servidor:  SZGZADWK16V01.satyatec.net
Address:  172.16.0.25

Nombre:  satyatec.es
Address:  172.16.0.29

> www.satyatec.es
Servidor:  SZGZADWK16V01.satyatec.net
Address:  172.16.0.25

Nombre:  www.satyatec.es
Address:  172.16.0.29


### ACCEDEMOS A 172.16.0.30 (SATYA/SATYA091) 

En principio el contenido wordpress esta bien desplegado aunque el servidor solo responde a las peticiones que se le envian a priori desde Firefox. Cuando realizamos una petición a satyatec.es Chrome no nos responde, aunque comprobamos desde un terminal móvil que la página se visualiza correctamente. Desde Firefox no tenemos ningun problema desde nuestro equipo. Accedemos a la base de datos y comprobamos que nuestro wordpress apunta no a una dirección ip, si no directamente al dominio satyatec.es (satya_wp > wp_options >  select * from wp_options WHERE option_name IN ('siteurl', 'home'); comprobamos que si realizamos busquedas por ip desde Chrome, el servidor si nos responde pero no llega a cargar todo el contenido o los modulos de nuestro wordpress (p.ej.: https://172.16.0.29/wordpress). 

Al estar ligado a un dominio, no tendría que haber problemas ya que ambas zonas son autoritativas para satyatec.es, tanto el 172.16.0.29 como el 172.16.0.30. De querer cambiar el resultado, deberiamos atender probablemente a la configuración de resolución del propio dominio en el hosting en el que se encuentre (desconocemos si esta en el propio Wordpress o en un registrador independiente). Desde la base de datos, no se aprecian mas errores. 

Observando la estructura de ficheros, wordpress esta bien desplegado y apache2 interpreta correctamente el contenido del mismo. No hay errores cuando se observa el estado del servicio (service apache2 status). A través de los logs de snapshots, obsrvamos que siempre apuntan a la dirección ip 172.16.0.29. La configuración del fichero config.php es la básica y tampoco se observan errores; además estan deshabilitados los registros de errores. 

Uno de los posibles problemas, de no venir por direccionamiento, puede ser por la incompatibilidad de la versión PHP con Google Chrome, ya que PHP5.6 fue "descatalogado" en 2018. 


### ACCESO A BD (soportesatya)

```sql
mysql> select * FROM wp_options Where option_name IN ('home', 'siteurl');
+-----------+-------------+---------------------+----------+
| option_id | option_name | option_value        | autoload |
+-----------+-------------+---------------------+----------+
|         2 | home        | https://satyatec.es | yes      |
|         1 | siteurl     | https://satyatec.es | yes      |
+-----------+-------------+---------------------+----------+
2 rows in set (0.00 sec)

.
```

### CAMBIAMOS LOS VALORES POR DEFECTO PARA QUE APUNTEN A LA PROPIA MAQUINA

```sql
UPDATE wp_options SET option_value = 'https://172.16.0.30' WHERE option_name IN ('home','siteurl');
SELECT option_value, option_name FROM wp_options WHERE option_name IN ('home', 'siteurl');

CREATE VIEW datos AS
    -> select option_value, option_name FROM wp_options WHERE option_name IN ('home', 'siteurl');
Query OK, 0 rows affected (0.00 sec)
```

Fuente: https://isabelcastillo.com/fix-local-ip-address-changed-cant-log-into-wordpress-in-ubuntu-lamp
Fuente: https://fututel.com/es/tutoriales-guias-manuales-videotutoriales/2883-cambiar-direccion-ip-a-wordpress-en-mysql-base-de-datos


### ACCESO A WORDPRESS (equipoe)

* Versión actual de WP - 	5.3.2 --> 5.7	[OK]
* versión actual de PHP - 7.2.1 --> 7.4.7	[OK]

Fuente: https://www.hostinger.es/tutoriales/como-actualizar-wordpress

Pendiente de actualizar versión en Wordpress, ya que sigue detectando:

* Versión actual de MySQL - 8.0.22
* Versión WordPress PHP	- 5.6.40

### Actualización WP
Soltamos directamente los contenidos del nuevo wp-admin y wp-includes. Reiniciamos el servicio de apache y probamos a cargar la web. Actualizamos base de datos, y en la página principal vemos que ya aparece la nueva versión de Wordpress.

### Actualización PHP
Actualizamos mediante el paquete de Linux a PHP7.4 y procedemos a eliminar PHP5.6; instalamos dos plugins adicionales para controlar la compatibilidad y la gestion de actualizaciones. Ejecutamos la primera, y esperamos unos minutos. 

```

#### Estás Usando una Versión Obsoleta de WordPress con PHP 7+

Si estás usando una versión muy antigua de WordPress con una versión moderna de PHP, puedes encontrar este error porque PHP 7.0 desaprobó la extensión MySQL en favor de MySQLi. Es decir, PHP 7+ ya no incluye la extensión MySQL que WordPress está buscando.

Las versiones modernas de WordPress usarán en su lugar las nuevas extensiones PDO_MySQL o MySQLi. Sin embargo, las versiones antiguas de WordPress (por debajo de la versión 3.9) podrían causar este problema.
```

[Cómo Arreglar el Error «Parece que Tu Instalación de PHP no Cuenta con la Extensión de MySQL, Necesaria para Hacer Funcionar WordPress» ](https://kinsta.com/es/base-de-conocimiento/error-extension-mysql-wordpress-falta/#3-comprueba-si-la-extensin-de-mysql-est-instalada)

Añadimos un fichero phpinfo.php, para que nos muestre todas las caracteristicas pero no se llega a visualizar en la web. No funciona. Intentamos copiar los modulos previos a PHP7.4 pero WP no detecta la base de datos, al parecer siguen faltando modulos o PHP los está bloqueando directamente. 

Exportamos la base de datos para revisar la codificación en local. Se generan copias de seguridad, del estilo backup_XXXX de php, wordpress y SQL. 

```sql
 mysqldump -u soportesatya -p --add-drop-database --create-options --lock-tables --verbose --all-databases > backup_db_wp-satya.sql

/*y luego para importarlo en local*/

mysql -u root -p
MariaDB [none]> source C:\Users\Guest\wp\backup_db_wp-satya.sql
```

El error que esta siendo producido puede estar provocado bien por el uso de un php obsoleto no orientado a objetos, o bien por la propia estructura definida a la hora de construir la base de datos. 

| Base de datos | WORDPRESS |
|-- | -- |
| Extensión | mysqli |
| Versión del servidor | 8.0.22-0ubuntu0.20.04.2 |
| Versión del cliente | mysqlnd 5.0.11-dev - 20120503 - $Id: 76b08b24596e12d4553bd41fc93cccd5bac2fe7a $ |
| Mapa de caracteres de la base de datos | utf8mb4 |
| Compilación de la base de datos | utf8mb4_unicode_520_ci |

Actualizamos todos los paquetes php y en consecuencia mysqld
```
*** mysqld.cnf (Y/I/N/O/D/Z) [default=N] ? D
--- /etc/mysql/mysql.conf.d/mysqld.cnf  2020-05-25 09:34:40.257451976 +0000
+++ /etc/mysql/mysql.conf.d/mysqld.cnf.dpkg-new 2020-11-26 12:03:33.000000000 +00>
@@ -28,7 +28,8 @@
 #
 # Instead of skip-networking the default is now to listen only on
 # localhost which is more compatible and is not less secure.
-bind-address           = 0.0.0.0
+bind-address           = 127.0.0.1
+mysqlx-bind-address    = 127.0.0.1
 #
 # * Fine Tuning
 #
```

Mantenemos el fichero de configuración por defecto, esté o no obsoleto. Una vez actualizado, probaremos de nuevo a cambiar la versión de PHP y si sigue sin funcionar, probaremos a exportar una copia del sitio, para tratar directamente con el codigo en local. Exportamos todo el sitio wordpress y lo volcamos en local. 

### Creación de usuario administrador WP
```sql

mysql> INSERT INTO wp_satya.wp_users(user_login, user_pass) VALUES('atorrellas', 'def123*');
Query OK, 1 row affected (0.36 sec)

mysql> UPDATE wp_satya.wp_users SET user_email = 'torrellasmara20r2@tiempos.org' WHERE user_login = 'atorrellas';
Query OK, 1 row affected (0.38 sec)

/*Comprobamos que se ha generado correctamente y modificamos la tabla wp_usermeta para añadirle el rol de administrador. */

mysql> SELECT user_login, user_email, user_pass FROM wp_satya.wp_users WHERE user_login = 'atorrellas';
+------------+-------------------------------+-----------+
| user_login | user_email                    | user_pass |
+------------+-------------------------------+-----------+
| atorrellas | torrellasmara20r2@tiempos.org | def123*   |
+------------+-------------------------------+-----------+
1 row in set (0.00 sec)

mysql> use wp_satya
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed

mysql> INSERT INTO wp_usermeta(user_id, meta_key, meta_value) VALUES (30, 'wp_capabilities', 'a:1:{s:13:"administrator";s:1:"1″;}');
Query OK, 1 row affected (0.36 sec)

```



## Creación de nuevo sitio

Creamos un nuevo sitio local con nuestras credenciales: **torrellasmara20r2 - 0F2sMz8hrT)#wo4llR**.
* Volcaremos la base de datos extraida
* Volcaremos todas las entradas y plugins configurados
* Comprobaremos la integridad de la página

Extraemos los ficheros de /var/www/html/wordpress/wp-content/themes para actualizar los temas en nuestro nuevo sitio, a través de un programa FTP. Los incluimos en la raíz, en nuestro caso C:\mysql\htdocs\wordpress\wp-content\themes. Podemos dejar directamente la carpeta con los ficheros descomprimidos, o desde la interfaz web, desde Temas > Añadir, implementar un fichero.zip. 

Tener cuidado a la hora de exportar toda la base de datos, puesto que algunos registro o inserciones pueden no ser grabadas en el fichero; además, si la máquina real apunta a un nuevo sitio o dominio, tendrá que ser modificado tanto el fichero wp-config.php como la base de datos principal _options_ > _siteurl_ y _home_. 

Tras varios intentos, habiendo generado de nuevo el sitio de Satya, nuestra servicio mysql cae y elimina toda la carpeta _data_ en la que se incluia toda la información de nuestro Wordpress _SATYA V2_ , decidimos entonces, desactivar el plugin _Disable All WordPress Updates_ para gestionar las actualizaciones desde _Easy Updates Manager_ . Actualizamos el Core de WP a la versión mas actual 5.7.2 con la que ya nos sale el aviso para actualizar como minimo a la versión de php7.4. 

Cambiamos el nombre de lservidor para poder acceder mediante web, sin tener conocimiento de la ip que se le ha asignado. 

```
echo 'srvsatya' > /etc/hostname
```

Del mismo modo añadimos dicho nombre al fichero de _hosts. _

```
atorrellas@srvsatya:/var$ sudo cat /etc/hosts
127.0.0.1       localhost
127.0.1.1       srvsatya srvSatya wp-satya

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
atorrellas@srvsatya:/var$
```

Actualizamos el sistema y reiniciamos. Antes de empezar y generar un nuevo sitio, vamos a subir a la plataforma cdmon nuestro sitio de pruebas, para comprobar con que versiones de PHP, Apache y SQL  es viable el proyecto de servicio web satyatec.es. 

### PAGINA DE PRUEBAS www.satyatec.org.mialias.net

Subimos el paquete.zip generado por el plugin _Duplicator_ y lo cargamos mediante SFTP con el programa WinSCP a la dirección 185.66.41.147 en /web/. Accedemos comprobamos las versiónes de cada modulo implementadas y arrancamos el instalador con el fichero _installer.php_ .

* Creamos una base de datos _satya-wp_ y un usuarios con todos los permisos para esa tabla. Cogemos los datos proporcionados por el hosting
** mysatyatec / .1LHLAD2

* Desplegamos la aplicación, seguimos con todo el procedimiento y comprobamos que se haya implementado correctamente. 

En efecto la aplicación ha sido implementada de forma correcta, exceptuando el plugin _wp-rocket_ que emite errores por haberse utilizado en su momento, rutas relativas referentes al servidor de origen. 


### Instalacion de servidor de pruebas

Instalamos y configuramos un servidor Ubuntu Server 18 y lo actuaizamos a la versión 20.04; procedemos a instalar los siguientes modulos:

* Apache2

* Mysql-server y postgre-sqlserver-dev-all

* php7.4

Accedemos a mysql mediante _sudo mysql_ y creamos un nuevo usuario de acceso a nuestro servidor BBDD

```sql

mysql> CREATE USER atorrellas IDENTIFIED BY 'def123*';
Query OK, 0 rows affected (0.41 sec)


mysql> GRANT ALL PRIVILEGES ON *.* TO 'atorrellas' WITH GRANT OPTION;
Query OK, 0 rows affected (0.06 sec)


mysql> CREATE VIEW mysql.usuarios AS select User, Host FROM mysql.user;
Query OK, 0 rows affected (0.37 sec)


mysql> select * from mysql.usuarios;
+------------------+-----------+
| User             | Host      |
+------------------+-----------+
| atorrellas       | %         |
| debian-sys-maint | localhost |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
6 rows in set (0.00 sec)


```

Instalamos zip para que el sistema pueda descomprimir la copia generada por WordPress, WP en adelante, y accederemos a nuestra servidor via web http://srvsatya/installer.php

´´´

atorrellas@srvsatya:/etc/php/7.4$ sudo apt install zip

´´´

Descomprimimos el fichero, seguimos el proceso pero enseguida se nos indica que la extensión para mysqli no está habilitada. Revisamos manualmente los ficheros siguientes en busca de funciones obsoletas de MySQL no orientado a objetos:
* /web/satya-web/wp-content/plugins/wp-staging/Service/Adapter
** Database
** DatabaseArticleDto

* /web/satya-web/wp-content/plugins/wp-staging/Backend/Modules/Jobs

Finalmente optamos por ejecutar el comando _grep -r "mysql_connect" /var/www/html"_ e ir a los ficheros especificos

```

atorrellas@srvsatya:/var/www/html$ grep -r "mysqli_connect" .
./dup-installer/classes/class.s3.func.php:            $dbConnError = (mysqli_connect_error()) ? 'Error: '.mysqli_connect_error() : 'Unable to Connect';
./dup-installer/classes/class.db.php:                $dbh       = @mysqli_connect('localhost', $username, $password, $dbname, null, $url_parts['path']);
./dup-installer/classes/class.db.php:                    $dbh = @mysqli_connect($host, $username, $password, $dbname, $port);
./dup-installer/classes/class.db.php:                    $dbh = @mysqli_connect($host, $username, $password, $dbname);
./dup-installer/classes/class.db.php:                DUPX_Log::info('DATABASE CONNECTION ERROR: '.mysqli_connect_error().'[ERRNO:'.mysqli_connect_errno().']');
./dup-installer/classes/class.db.php:     * @param resource $dbh     The resource given by mysqli_connect
./dup-installer/ctrls/ctrl.s2.dbinstall.php:        ($this->dbh) or DUPX_Log::error(ERR_DBCONNECT.mysqli_connect_error());
./dup-installer/ctrls/ctrl.s2.dbtest.php:                               $test['info']    = (mysqli_connect_error())
./dup-installer/ctrls/ctrl.s2.dbtest.php:                                      ? "{$msg}. The server error response was: <i>" . htmlentities(mysqli_connect_error()) . '</i>'
./dup-installer/views/view.s1.base.php:$req['20'] = function_exists('mysqli_connect')                                           ? 'Pass' : 'Fail';
./dup-installer/views/view.s1.base.php:                         the function_exists('mysqli_connect') call.</i>
atorrellas@srvsatya:/var/www/html$

```

Configuramos el servidor con una ip estática ya que comprobamos que cada día obtiene una distinta y no estamos pudiendo llegar por nombre.
```

atorrellas@srvsatya:~$ sudo cat /etc/netplan/00-installer-config.yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    enp0s3:
      addresses:
        - 172.16.1.189/23
      gateway4: 172.16.0.254
      nameservers:
        search: [satyatec.es]
        addresses: [8.8.8.8, 1.1.1.1]
  version: 2
atorrellas@srvsatya:~$

```

Reiniciamos el equipo, comprobamos que tiene conectividad y procedemos a instalar el paquete _php7.4-mysql_. Reiniciamos el servicio de apache y cargamos de nuevo la página web, comprobando que ya tenemos la validación de _Duplicator_ para seguir con la instalación. Creamos una nueva base de datos y un usuario con permisos globales sobre esta bd. 

```sql

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| wp_satya           |
+--------------------+
5 rows in set (0.00 sec)


mysql> CREATE USER 'soportesatya' IDENTIFIED BY 'P4r4d0x!';
Query OK, 0 rows affected (0.59 sec)

mysql> GRANT ALL PRIVILEGES ON wp_satya TO soportesatya;
ERROR 1046 (3D000): No database selected
mysql> GRANT ALL PRIVILEGES ON wp_satya.* TO soportesatya;
Query OK, 0 rows affected (0.39 sec)



```

Acabamos la instalación, borramos todos los ficheros del plugin y probamos a acceder al wordpress comprobando que ha sido correctamente implementado, aunque no podemos de momento, acceder al panel de administración. Añadimos un nuevo usuario a la bd para acceder al panel

```sql

mysql> INSERT INTO wp_satya.wp_users(user_login, user_pass) VALUES('atorrellas', 'def123*');
Query OK, 1 row affected (0.36 sec)

mysql> UPDATE wp_satya.wp_users SET user_email = 'torrellasmara20r2@tiempos.org' WHERE user_login = 'atorrellas';
Query OK, 1 row affected (0.38 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT user_login, user_email, user_pass FROM wp_satya.wp_users WHERE user_login = 'atorrellas';
+------------+-------------------------------+-----------+
| user_login | user_email                    | user_pass |
+------------+-------------------------------+-----------+
| atorrellas | torrellasmara20r2@tiempos.org | def123*   |
+------------+-------------------------------+-----------+
1 row in set (0.00 sec)

```

Procedamos ahora a instalar los plugins que nos aparecen en el directorio principal

* Fallo al importar «BackWPup»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «BruteGuard – Brute Force Login Protection»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «CC Cookie Consent»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Contact Form 7»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Contact Form 7 - reCAPTCHA»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Custom Login Page Customizer»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Disable All WordPress Updates»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Display PHP Version»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Duplicator»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Easy Updates Manager»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «File Manager Advanced»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «File Manager Advanced Shortcode»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Fusion Core»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Google XML Sitemaps»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «LayerSlider WP»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «MCE Table Buttons»: tipo de contenido no válido wpephpcompat_jobs
* Bloque reutilizable «Bloque reutilizable sin título» ya existe.
* Fallo al importar «Official StatCounter Plugin»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Post SMTP»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Slider Revolution»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Super Progressive Web Apps»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «Timeline Twitter Feed»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «TinyMCE Advanced»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «TinyMCE and TinyMCE Advanced Professsional Formats and Styles»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «WP File Manager»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «WP Missed Schedule Posts»: tipo de contenido no válido wpephpcompat_jobs
* Fallo al importar «WP Rocket»: tipo de contenido no válido wpephpcompat_jobs

´´´code

atorrellas@srvsatya:/var/www/html/wp-content/plugins$ ls
LayerSlider	[plugin Avada]        	pandora-fms-wp		[OK]
advanced-tinymce-configuration	[OK]     
backwpup   	[OK]                    revslider - slider revolution [error critico]
bruteguard	[OK]			rocket-footer-js
complianz-gdpr	[OK]			search-placeholder-avada[OK]
contact-form-7	[OK]			smart-slider-3		[OK]
custom-twitter-feeds   		[OK]    stops-core-theme-and-plugin-updates
disable-wordpress-updates	[OK]    tinymce-advanced	[OK]
duplicator	[OK]			tinymce-custom-styles	[OK]
file-manager-advanced		[OK]	under-construction-page	[OK]
fusion                                   
fusion-core	[OK]                     wordpress-2-step-verification	[OK]
google-sitemap-generator	[OK]     wordpress-importer
mce-table-buttons		[OK]                
missed-schedule-post-publisher   
missed-scheduled-posts-publisher        wp-file-manager	[OK]
ml-slider	[OK]			wp-staging	[OK]
official-statcounter-plugin-for-wordpress[OK]  wpcf7-recaptcha	[OK]

´´´

Y una vez instalados y comprobado su funcionamiento, procedemos a importar las entradas y articulos del WP anterior. Como ya habia algunas entradas o bloques definidos, se duplican algunas partes de la página web, aunque vemos que la pagina principal funciona correctamente sin redirigir a las entradas que hay publicadas. 

Volvemos al servidor secundario, borramos todas las versiones de php, instalamos php7.4 con su complemento php7.4-mysql (en realidad hemos instalado todos los complementos _sudo apt install php7.4-mysql*_) quitamos todos los plugins, renombrando la carpeta principal _plugins_ => _old-plugins_ . Comprobamos y detectamos el plugin con problemas, que pertenece a la plantilla Avada, lo eliminamos y procedemos a volcar todo el contenido en nuestra maquina local _srvsatya_. 

Comprobamos que es uno de los plugins de la plantilla Avada, llamado REVOLUTION SLIDER, que provoca la no visibilidad del logo y el menu en la página principal. Con la ultima versión de php no parece haber compatibilidad. Conseguimos solventar el fallo aplicando las medidas que se redactan en el siguiente [enlace](http://mialtoweb.es/activacion-de-revolution-slider-con-php-7/)

Atención ya que tras cada carga de nuestro sitio, puede ser que la correción aplicada se deshaga. Creamos para ello un backup_plugins en el mismo directorio en el que se encuentran */var/www/html/wordpress/wp-content/plugins/*

### RESUMEN DEL ESTADO DEL PROYECTO a 21/05/2021 - 12:21

* La página web hospedada en un hosting gratuito, funciona correctamente a excepción de los plugins de pago.
* La página web desplegada en 172.16.1.189 con los plugins correspondientes, no muestra contenido, por lo que no funciona como es debido. 
* La web del servidor 172.16.0.30 da servicio, pero no da acceso al panel de administración Wordpress. 

### Revisión configuración Apache2

Revisamos los mods-enable y sites-enabled, actualizamos todos los modulos cargados y añadimos las directivas que tiene el servidor secundario. Como esta distribuido en jerarquias de directorios diferentes, generamos una nueva carpeta _wordpress_ y realizamos una copia de todo en ese directorio. Reiniciamos el servicio de apache2. 

### Conclusión

Finalmente hemos conseguido actualizar toda la página web del servidor secundario a la ultima versión de WP, junto con PHP7.4 y MySQL 8; se han solventado los problemas de compatibilidad con MySQL y con el plugin del tema Avada _Slider Revolution_ modificando para ello el propio codigo del plugin, en concreto los ficheros base y base-admin.php. A partir de ahora, ya podemos implementar un plugin e-commerce para desarrollar nuestra página web. 

Actualmente la pagina de nuestro servidor funciona parcialmente debido a las rutas relativas establecidas, probablemente, en el sitio original. Tampoco reconoce el certificado SSL. 
* reCaptcha:  No tenemos las credenciales para la llave de encriptación
* SSL: No tenemos el certificado para poder importarlo de nuevo en la página web.

Proseguimos con el desarrollo de la tienda online, mediante CMS. 

 