# Construcción E-Commerce Satya.es

Previamente a la construcción del sitio, quedan dos minimos temas pendientes que revisar en las páginas web: 
* La carga de imagenes
* El cambio de llave para Google reCaptcha
* Certificado SSL

## Prerequisitos

Para poder avanzar con el desarrollo es necesario haber recuperado las imagenes en el servicio HTTP, la seguridad SSL y el cambio de llave para Google reCaptcha. 

### Carga de imagenes

Procedemos a revisar la carga de imagenes directamente por FTP para comprobar que estan subidas correctamente en la ruta _upload_ y cargamos por si acaso los plugins antiguos junto con las modificaciones explicadas en el log previo. Revisamos que la URL establecida sea la correcta y que los medios no han sido corrompidos. Intentamos establecer las imagenes manualmente, pero no se ven reflejadas en la página a excepción del icono web. 
* [How To Fix “Failed To Load Resource” Error In WordPress](https://www.wpbeginner.com/wp-tutorials/how-to-fix-failed-to-load-resource-error-in-wordpress/)

Hacemos varias cargas de los plugins, tanto desde copias de seguridad realizadas, como de los que estan funcionando correctamente en nuestro servidor local. Comprobamos los datos y las configuraciones tanto de los ficheros .htaccess, como de los ficheros.conf de cada extensión. Comprobamos que desde la consola de los navegadores web el error resultante siempre es el mismo _Failed to load resource: net::ERR_CONNECTION_REFUSED._ 

Cabe destacar que dicho error solo se produciría aparentemente, a través del navegador Google Chrome. Comprobamos con la siguiente [herramienta](https://downforeveryoneorjustme.com/satyatec.es) que el servicio web esta levantado. Cargamos una imagen previa y comprobamos que funciona. 

### reCaptcha

Comprobamos que al no tener la cuenta autorizada no podemos comprobar si la clave de dominio para ese host especifico, ha variado o no. 

### Certificado SSL

Entendemos que al haber gestionado la pagina desde tantos puntos, alguno de los plugins que acompañaba al certificado SSL ha variado. Comprobamos con un plugin diferente que el certificado SSL si que es detectado, y si que es valido hasta proximos meses, por lo que se descarta un error de caducidad. En principio, con el certificado disponible, podriamos volver a importarlo y configurar el sitio para que sea accesible desde https.

De todos modos se plantea en dias sucesivos, enlazar la página web a un nuevo sitio, para evitar todos estos errores puesto que de momento, no se ha respetado la integridad de la página web con los diferentes plugins implementados tanto de maquetación como de funcionamiento, aunque esto suponga trabajar con una versión de PHP inferior a la recomendada. 

## Desarrollo de la instalación

Vamos a probar primero a instalar PrestaShop en nuestro hosting gratuito cdmon. Para ello desde el asistente del panel de adminitración, seleccionamos _Prestashop_ y le indicamos en que ruta ha de guardarse toda la información. 

Descargamos prestashop y lo descomprimimos en el directorio principal de apache, bajo una carpeta denominada _tienda_ /var/www/html/tienda. Antes de empezar con la instalación tendremos dar permisos de escritura a los usuarios e instalar varios paquetes adicionales: zip y php7.4-zip. 

Una vez hecho esto deberemos definir nuestro sitio en el servidor apache; para ello copiamos el fichero que hay por defecto y cambiamos los servidores, directorios y permisos para el nuevo sitio.

```

satya@ubuntu2004:/etc/apache2/sites-available$ cat prestashop.conf
<Directory /var/www/tienda/>
   AllowOverride All
   Options Indexes
   Require all granted
</Directory>
<VirtualHost *:80>
 ServerName satyatec.es
 DocumentRoot /var/www/html/tienda
 ErrorLog ${APACHE_LOG_DIR}/prestashop.error.log
 CustomLog ${APACHE_LOG_DIR}/prestashop.access.log combined
</VirtualHost>
satya@ubuntu2004:/etc/apache2/sites-available$
/*		Habilitamos el sitio, y reiniciamos el servicio */
satya@ubuntu2004:/etc/apache2/sites-available$ sudo a2ensite prestashop.conf
Enabling site prestashop.
To activate the new configuration, you need to run:
  systemctl reload apache2
satya@ubuntu2004:/etc/apache2/sites-available$ sudo systemctl reload apache2
```

Como tenemos problemas para implementar WP con PrestaShop, a partir de ahora PS, vamos a utilizar la extensión WooCommerce para ir desarrollando la página en primera instancia en el servidor local. Descargamos el plugin de la [pagina oficial](https://kinsta.com/es/blog/aumentar-tamano-maximo-subida-archivos-wordpress/#aumentar-el-tamao-mximo-de-los-archivos-en-wordpress-multisite), lo descomprimimos y subimos por FTP al directorio _ /var/www/html/wp-content/plugins _. 

Antes de implementar el plugin y empezar a desarrollar el sitio, vamos a realizar una copia de seguridad de nuestro fichero de configuración wp-config.php, y a guardar una copia de todo nuestro sitio con Duplicator que tendrá por nombre BACKUP_2021_05_31_SATYA_WEB_TIENDA. Aprovechamos para optimizar nuestra base de datos, incluyendo para ello la siguiente linea a nuestro fichero de configuración.
```
define('WP_ALLOW_REPAIR', true);
```

Para asegurarnos de que el proceso sale correctamente, descargamos una copia del contenido que reside en el servidor mediante conexión FTP. Ahora ya, con las copias previamente establecidas, vamos a proceder a instalar el plugin WooCommerce directamente desde el propio panel backend de WP. 
Instalarlo es muy sencillo, solamente basta con elegir el plugin, instalarlo y activarlo; una vez activado, deberemos rellenar algunos datos que servirán de base, para genera nuestra nueva tienda (nombre, dirección, unidad monetaria, etc). 

Al estar implementado con WP, la temática y los formatos de estilos mantendrán el formato de la plantilla que se tenga activa; solo hará falta entonces, vincular nuestra tienda en algún menú o bien, generar un enlace permanente a ésta. En nuestro caso se opta por incorporar un elemento al menu superior, denominado _Venta online_ .

Una vez que tenemos los datos minimos para que el plugin pueda oferecer información relevante de nuestro sitio, es el momento de empezar a configurar nuestros productos, incorporandolos a nuestra tienda, aportando información adicional no registrada en nuestra base de datos y eligiendo el estilo o el orden en el que se mostrarán, una vez implementados en nuestra página web. 

En nuestro servidor local hemos insertado un artículo manualmente para que se aprecie como quedaría el catalogo que verían los usuarios al acceder a nuestra sección de compra. Unavez obtenidos los productos a mostrar, procedemos a insertarlos uno a uno en la tabla, con sus atributos, dimensiones y etiquetas, y procedemos a configurar brevemente como se dispondran, y que referencias cruzadas mostrarán a los usuarios durante su compra. 

Al ser un sitio no especializado en la compra y dispensación, se deben aclarar dos cosas: que los usuarios no tendrán la posibilidad de registrarse, ya que no se contempla la afiliciación ni programa semejante; todo pedido se tramita directamente con la empresa, por lo que no hay necesidad de pago en el momento, siempre y cuando la empresa o representante del proveedor lo crea conveniente. Aún no habiendo programa de afiliación ni clientes como tal, los usuarios pueden valorar los productos con su perfil, verificando en todo momento el numero de pedido; para cualquier problema con el producto, existe el apartado Contacto, en el que se podrán resolver y aclarar aspectos del estado (estado, reclamaciones, dudas, etc). 

Si el cliente se precisa, se puede convertir la tienda en un panel de solicitudes en el que la empresa corporativa, en este caso Satya SL, puede mostrar al publico el tipo de tecnologías con las que trabaja sin necesidad de dispensar de los articulos directamente al cliente. Podemos orientar entonces esta tienda, como una venta de servicios, mejor que de articulos tanto físicos como digitales. 

Para poder cumplimentar una pasarela de pago, vamos a instalar los complementos Stripe y GoogleAnalytics desde la página oficial 
