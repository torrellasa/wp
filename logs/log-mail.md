#Enlaces

[Configurar oAuth2.0](https://help.dreamhost.com/hc/es/articles/360029702631-Habilitar-el-API-OAuth-2-para-usar-con-tu-plugin-Mail-SMTP-de-WordPress)

[Configurar Gmail OAuth](https://help.dreamhost.com/hc/es/articles/360029770912-Configuraci%C3%B3n-del-plugin-WP-Mail-SMTP-con-Gmail)

[Pag Principal GDeveloper](https://console.cloud.google.com/apis/credentials/domainverification?folder=&project=nodal-empire-315812)

[Stripe - Tarjeta Pasarela de PAgo](https://dashboard.stripe.com/test/connect/accounts/overview)

## Procedimiento

Con el objetivo de desarrollar un servicio de correo minimo, a través de la web (formularios de contacto, incidencias, etc), es necesario tener un servicio de correo propio, o bien, una cuenta por ejemplo de Google con la que vamos a explicar este apartado, que nos permita usar los servidores para el envío y recepción de mensajes. Con ésto ya a nuestro alcance, implementaremos la extensión POST SMTP que configuraremos con los datos recopilados anteriormente:
* Servidor SMTP
* Cuenta emisora
* Clave secreta y clave pública (Véase Anexo 9 - Google Developer)

No hemos necesitado mucho mas. 

### Google Developer - API Services

Iniciamos sesión con una cuenta de Google y creamos un proyecto. Dentro del proyecto en el apartado consentimiento, aceptamos que se vaya a hacer uso de nuestra a API con clientes internos de nuestra corporación. Una vez cumplimentado los datos de desarrollador y administrador, podremos crear un nuevo ID Cliente OAuth2, recoger sus credenciales e implementarlas en nuestro Wordpress. 

Una vez hecho esto, deberemos Vincular Google con WP, a través de la opción que reside en Post SMTP, y a continuación enviar un correo de prueba. 

Consultar de nuevo la API si queremos comprobar el uso que se le está haciendo. 



